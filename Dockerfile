FROM ubuntu:trusty
EXPOSE 2375

RUN apt-get update
RUN apt-get install -y apt-transport-https wget curl
RUN wget https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz 
RUN tar zxvf google-cloud-sdk.tar.gz 
RUN chmod +x ./google-cloud-sdk/install.sh
RUN google-cloud-sdk/bin/gcloud --quiet components update

#COPY gcloud-service-key.json /etc/gcloud-service-key.json
#RUN google-cloud-sdk/bin/gcloud auth activate-service-account --key-file /etc/gcloud-service-key.json

VOLUME ["/var/lib/docker"]

COPY docker.list.xenial /etc/apt/sources.list.d/docker.list
RUN apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
RUN apt-get update
RUN apt-get install -y docker-engine=1.10.3-0~trusty

COPY docker-entrypoint.sh /
RUN chmod +x docker-entrypoint.sh

ENTRYPOINT ["sh", "/docker-entrypoint.sh"]